const express = require('express');
const request = require('supertest');
const expressLoader = require('../../../src/loaders/express');

jest.mock('../../../src/config');

describe('tests for the healthcheck route', () => {
  it('Check health', async (done) => {
    const { app } = await expressLoader(express());

    request(app)
      .get('/v1/health')
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        expect(res.body.status).toBe('up');
        expect(res.body.env).toBe('test');
        expect(res.body.clusterEnv).toBe('test');
        return done();
      });
  });
});
